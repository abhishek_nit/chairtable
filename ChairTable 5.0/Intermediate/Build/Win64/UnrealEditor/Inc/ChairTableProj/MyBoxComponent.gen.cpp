// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChairTableProj/MyBoxComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBoxComponent() {}
// Cross Module References
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_UMyBoxComponent_NoRegister();
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_UMyBoxComponent();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent();
	UPackage* Z_Construct_UPackage__Script_ChairTableProj();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMyBoxComponent::execSetBoxMaterial)
	{
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBoxMaterial(Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMyBoxComponent::execBuild)
	{
		P_GET_STRUCT(FVector,Z_Param_Size);
		P_GET_UBOOL(Z_Param_CollisionEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Build(Z_Param_Size,Z_Param_CollisionEnabled);
		P_NATIVE_END;
	}
	void UMyBoxComponent::StaticRegisterNativesUMyBoxComponent()
	{
		UClass* Class = UMyBoxComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Build", &UMyBoxComponent::execBuild },
			{ "SetBoxMaterial", &UMyBoxComponent::execSetBoxMaterial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMyBoxComponent_Build_Statics
	{
		struct MyBoxComponent_eventBuild_Parms
		{
			FVector Size;
			bool CollisionEnabled;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Size;
		static void NewProp_CollisionEnabled_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_CollisionEnabled;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyBoxComponent_eventBuild_Parms, Size), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData)) };
	void Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled_SetBit(void* Obj)
	{
		((MyBoxComponent_eventBuild_Parms*)Obj)->CollisionEnabled = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled = { "CollisionEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MyBoxComponent_eventBuild_Parms), &Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyBoxComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyBoxComponent, nullptr, "Build", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::MyBoxComponent_eventBuild_Parms), Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyBoxComponent_Build()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMyBoxComponent_Build_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics
	{
		struct MyBoxComponent_eventSetBoxMaterial_Parms
		{
			const UMaterialInterface* NewMaterial;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_NewMaterial_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyBoxComponent_eventSetBoxMaterial_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyBoxComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyBoxComponent, nullptr, "SetBoxMaterial", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::MyBoxComponent_eventSetBoxMaterial_Parms), Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMyBoxComponent);
	UClass* Z_Construct_UClass_UMyBoxComponent_NoRegister()
	{
		return UMyBoxComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMyBoxComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMyBoxComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ChairTableProj,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMyBoxComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMyBoxComponent_Build, "Build" }, // 2680573064
		{ &Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial, "SetBoxMaterial" }, // 555580940
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object LOD Mobility Trigger" },
		{ "IncludePath", "MyBoxComponent.h" },
		{ "ModuleRelativePath", "MyBoxComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMyBoxComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMyBoxComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMyBoxComponent_Statics::ClassParams = {
		&UMyBoxComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMyBoxComponent()
	{
		if (!Z_Registration_Info_UClass_UMyBoxComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMyBoxComponent.OuterSingleton, Z_Construct_UClass_UMyBoxComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMyBoxComponent.OuterSingleton;
	}
	template<> CHAIRTABLEPROJ_API UClass* StaticClass<UMyBoxComponent>()
	{
		return UMyBoxComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyBoxComponent);
	struct Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyBoxComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyBoxComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMyBoxComponent, UMyBoxComponent::StaticClass, TEXT("UMyBoxComponent"), &Z_Registration_Info_UClass_UMyBoxComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMyBoxComponent), 2857346310U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyBoxComponent_h_1365515314(TEXT("/Script/ChairTableProj"),
		Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyBoxComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyBoxComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
