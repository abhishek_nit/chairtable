// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChairTableProj/MyChair.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyChair() {}
// Cross Module References
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_AMyChair_NoRegister();
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_AMyChair();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ChairTableProj();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
// End Cross Module References
	void AMyChair::StaticRegisterNativesAMyChair()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AMyChair);
	UClass* Z_Construct_UClass_AMyChair_NoRegister()
	{
		return AMyChair::StaticClass();
	}
	struct Z_Construct_UClass_AMyChair_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ChairMesh_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ChairMesh;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyChair_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ChairTableProj,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyChair_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MyChair.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "MyChair.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyChair_Statics::NewProp_ChairMesh_MetaData[] = {
		{ "Category", "MyChair" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyChair.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyChair_Statics::NewProp_ChairMesh = { "ChairMesh", nullptr, (EPropertyFlags)0x00100000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyChair, ChairMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyChair_Statics::NewProp_ChairMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyChair_Statics::NewProp_ChairMesh_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyChair_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "MyChair" },
		{ "ModuleRelativePath", "MyChair.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyChair_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyChair, Material), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyChair_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyChair_Statics::NewProp_Material_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyChair_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyChair_Statics::NewProp_ChairMesh,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyChair_Statics::NewProp_Material,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyChair_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyChair>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AMyChair_Statics::ClassParams = {
		&AMyChair::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyChair_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyChair_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyChair_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyChair_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyChair()
	{
		if (!Z_Registration_Info_UClass_AMyChair.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AMyChair.OuterSingleton, Z_Construct_UClass_AMyChair_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AMyChair.OuterSingleton;
	}
	template<> CHAIRTABLEPROJ_API UClass* StaticClass<AMyChair>()
	{
		return AMyChair::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyChair);
	struct Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AMyChair, AMyChair::StaticClass, TEXT("AMyChair"), &Z_Registration_Info_UClass_AMyChair, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AMyChair), 3513719191U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_84020695(TEXT("/Script/ChairTableProj"),
		Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
