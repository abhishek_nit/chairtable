// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHAIRTABLEPROJ_MyChair_generated_h
#error "MyChair.generated.h already included, missing '#pragma once' in MyChair.h"
#endif
#define CHAIRTABLEPROJ_MyChair_generated_h

#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_SPARSE_DATA
#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_RPC_WRAPPERS
#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyChair(); \
	friend struct Z_Construct_UClass_AMyChair_Statics; \
public: \
	DECLARE_CLASS(AMyChair, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(AMyChair)


#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyChair(); \
	friend struct Z_Construct_UClass_AMyChair_Statics; \
public: \
	DECLARE_CLASS(AMyChair, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(AMyChair)


#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyChair(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyChair) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyChair); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyChair); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyChair(AMyChair&&); \
	NO_API AMyChair(const AMyChair&); \
public:


#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyChair(AMyChair&&); \
	NO_API AMyChair(const AMyChair&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyChair); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyChair); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyChair)


#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_12_PROLOG
#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_SPARSE_DATA \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_RPC_WRAPPERS \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_INCLASS \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_SPARSE_DATA \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_INCLASS_NO_PURE_DECLS \
	FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAIRTABLEPROJ_API UClass* StaticClass<class AMyChair>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_ChairTable_5_0_Source_ChairTableProj_MyChair_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
