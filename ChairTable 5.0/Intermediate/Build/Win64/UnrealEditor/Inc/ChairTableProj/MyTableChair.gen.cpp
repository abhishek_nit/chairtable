// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChairTableProj/MyTableChair.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyTableChair() {}
// Cross Module References
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_AMyTableChair_NoRegister();
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_AMyTableChair();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ChairTableProj();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_UMyBoxComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AMyTableChair::execUpdateLocations)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateLocations();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetCounterClockwiseCorner)
	{
		P_GET_OBJECT(UProceduralMeshComponent,Z_Param_CurrentCorner);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UProceduralMeshComponent**)Z_Param__Result=P_THIS->GetCounterClockwiseCorner(Z_Param_CurrentCorner);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetClockwiseCorner)
	{
		P_GET_OBJECT(UProceduralMeshComponent,Z_Param_CurrentCorner);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UProceduralMeshComponent**)Z_Param__Result=P_THIS->GetClockwiseCorner(Z_Param_CurrentCorner);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetCorners)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UMyBoxComponent*>*)Z_Param__Result=P_THIS->GetCorners();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetTableHeight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetTableHeight();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetTableSize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector2D*)Z_Param__Result=P_THIS->GetTableSize();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execGetOppositeCorner)
	{
		P_GET_OBJECT(UProceduralMeshComponent,Z_Param_CurrentCorner);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UProceduralMeshComponent**)Z_Param__Result=P_THIS->GetOppositeCorner(Z_Param_CurrentCorner);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyTableChair::execSetCornerWorldLocation)
	{
		P_GET_OBJECT(UProceduralMeshComponent,Z_Param_Corner);
		P_GET_STRUCT(FVector,Z_Param_NewWorldLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetCornerWorldLocation(Z_Param_Corner,Z_Param_NewWorldLocation);
		P_NATIVE_END;
	}
	void AMyTableChair::StaticRegisterNativesAMyTableChair()
	{
		UClass* Class = AMyTableChair::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetClockwiseCorner", &AMyTableChair::execGetClockwiseCorner },
			{ "GetCorners", &AMyTableChair::execGetCorners },
			{ "GetCounterClockwiseCorner", &AMyTableChair::execGetCounterClockwiseCorner },
			{ "GetOppositeCorner", &AMyTableChair::execGetOppositeCorner },
			{ "GetTableHeight", &AMyTableChair::execGetTableHeight },
			{ "GetTableSize", &AMyTableChair::execGetTableSize },
			{ "SetCornerWorldLocation", &AMyTableChair::execSetCornerWorldLocation },
			{ "UpdateLocations", &AMyTableChair::execUpdateLocations },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics
	{
		struct MyTableChair_eventGetClockwiseCorner_Parms
		{
			const UProceduralMeshComponent* CurrentCorner;
			UProceduralMeshComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentCorner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CurrentCorner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_CurrentCorner = { "CurrentCorner", nullptr, (EPropertyFlags)0x0010000000080082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetClockwiseCorner_Parms, CurrentCorner), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetClockwiseCorner_Parms, ReturnValue), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_CurrentCorner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetClockwiseCorner", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::MyTableChair_eventGetClockwiseCorner_Parms), Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_GetCorners_Statics
	{
		struct MyTableChair_eventGetCorners_Parms
		{
			TArray<UMyBoxComponent*> ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMyBoxComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetCorners_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetCorners", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::MyTableChair_eventGetCorners_Parms), Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetCorners()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetCorners_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics
	{
		struct MyTableChair_eventGetCounterClockwiseCorner_Parms
		{
			const UProceduralMeshComponent* CurrentCorner;
			UProceduralMeshComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentCorner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CurrentCorner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_CurrentCorner = { "CurrentCorner", nullptr, (EPropertyFlags)0x0010000000080082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetCounterClockwiseCorner_Parms, CurrentCorner), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_CurrentCorner_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetCounterClockwiseCorner_Parms, ReturnValue), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_CurrentCorner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetCounterClockwiseCorner", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::MyTableChair_eventGetCounterClockwiseCorner_Parms), Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics
	{
		struct MyTableChair_eventGetOppositeCorner_Parms
		{
			const UProceduralMeshComponent* CurrentCorner;
			UProceduralMeshComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentCorner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CurrentCorner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_CurrentCorner_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_CurrentCorner = { "CurrentCorner", nullptr, (EPropertyFlags)0x0010000000080082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetOppositeCorner_Parms, CurrentCorner), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_CurrentCorner_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_CurrentCorner_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetOppositeCorner_Parms, ReturnValue), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_CurrentCorner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetOppositeCorner", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::MyTableChair_eventGetOppositeCorner_Parms), Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetOppositeCorner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetOppositeCorner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics
	{
		struct MyTableChair_eventGetTableHeight_Parms
		{
			float ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetTableHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetTableHeight", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::MyTableChair_eventGetTableHeight_Parms), Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetTableHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetTableHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics
	{
		struct MyTableChair_eventGetTableSize_Parms
		{
			FVector2D ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventGetTableSize_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "GetTableSize", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::MyTableChair_eventGetTableSize_Parms), Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_GetTableSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_GetTableSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics
	{
		struct MyTableChair_eventSetCornerWorldLocation_Parms
		{
			UProceduralMeshComponent* Corner;
			FVector NewWorldLocation;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Corner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Corner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_NewWorldLocation_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_NewWorldLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_Corner_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_Corner = { "Corner", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventSetCornerWorldLocation_Parms, Corner), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_Corner_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_Corner_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_NewWorldLocation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_NewWorldLocation = { "NewWorldLocation", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyTableChair_eventSetCornerWorldLocation_Parms, NewWorldLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_NewWorldLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_NewWorldLocation_MetaData)) };
	void Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MyTableChair_eventSetCornerWorldLocation_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MyTableChair_eventSetCornerWorldLocation_Parms), &Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_Corner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_NewWorldLocation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "SetCornerWorldLocation", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::MyTableChair_eventSetCornerWorldLocation_Parms), Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyTableChair, nullptr, "UpdateLocations", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyTableChair_UpdateLocations()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMyTableChair_UpdateLocations_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AMyTableChair);
	UClass* Z_Construct_UClass_AMyTableChair_NoRegister()
	{
		return AMyTableChair::StaticClass();
	}
	struct Z_Construct_UClass_AMyTableChair_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TableMaterial_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_TableMaterial;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyTableChair_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ChairTableProj,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMyTableChair_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMyTableChair_GetClockwiseCorner, "GetClockwiseCorner" }, // 2840233899
		{ &Z_Construct_UFunction_AMyTableChair_GetCorners, "GetCorners" }, // 1468877763
		{ &Z_Construct_UFunction_AMyTableChair_GetCounterClockwiseCorner, "GetCounterClockwiseCorner" }, // 3775450993
		{ &Z_Construct_UFunction_AMyTableChair_GetOppositeCorner, "GetOppositeCorner" }, // 1235734376
		{ &Z_Construct_UFunction_AMyTableChair_GetTableHeight, "GetTableHeight" }, // 69485728
		{ &Z_Construct_UFunction_AMyTableChair_GetTableSize, "GetTableSize" }, // 1824477880
		{ &Z_Construct_UFunction_AMyTableChair_SetCornerWorldLocation, "SetCornerWorldLocation" }, // 2707255933
		{ &Z_Construct_UFunction_AMyTableChair_UpdateLocations, "UpdateLocations" }, // 775278324
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyTableChair_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyTableChair.h" },
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyTableChair_Statics::NewProp_TableMaterial_MetaData[] = {
		{ "ModuleRelativePath", "MyTableChair.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyTableChair_Statics::NewProp_TableMaterial = { "TableMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyTableChair, TableMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyTableChair_Statics::NewProp_TableMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyTableChair_Statics::NewProp_TableMaterial_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyTableChair_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyTableChair_Statics::NewProp_TableMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyTableChair_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyTableChair>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AMyTableChair_Statics::ClassParams = {
		&AMyTableChair::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMyTableChair_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyTableChair_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyTableChair_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyTableChair_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyTableChair()
	{
		if (!Z_Registration_Info_UClass_AMyTableChair.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AMyTableChair.OuterSingleton, Z_Construct_UClass_AMyTableChair_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AMyTableChair.OuterSingleton;
	}
	template<> CHAIRTABLEPROJ_API UClass* StaticClass<AMyTableChair>()
	{
		return AMyTableChair::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyTableChair);
	struct Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyTableChair_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyTableChair_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AMyTableChair, AMyTableChair::StaticClass, TEXT("AMyTableChair"), &Z_Registration_Info_UClass_AMyTableChair, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AMyTableChair), 2698548411U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyTableChair_h_2577467616(TEXT("/Script/ChairTableProj"),
		Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyTableChair_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_ChairTable_5_0_Source_ChairTableProj_MyTableChair_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
