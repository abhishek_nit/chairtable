// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MyBoxComponent.generated.h"

/**
 * 
 */
UCLASS()
class CHAIRTABLEPROJ_API UMyBoxComponent : public UProceduralMeshComponent
{
	GENERATED_BODY()
public:

	UMyBoxComponent(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
	void Build(const FVector Size, bool CollisionEnabled);

	void Build(const FVector Size, bool CollisionEnabled, const TArray<FLinearColor> LinearColors, const TArray<FProcMeshTangent> MeshTangents);

	UFUNCTION()
	void SetBoxMaterial(const UMaterialInterface* const NewMaterial);
};
