// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyBoxComponent.h"
#include "ProceduralMeshComponent.h"
#include "Materials/Material.h"
#include "MyChair.generated.h"

UCLASS(Blueprintable)
class CHAIRTABLEPROJ_API AMyChair : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMyChair();

	static constexpr  float CHAIR_SQUARE_SIZE = 30;
	static constexpr  float CHAIR_SQUARE_THICKNESS = 2;
	static constexpr  float CHAIR_LEG_HEIGHT = 45;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UProceduralMeshComponent* ChairMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UMaterial* Material;
};
