// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "Camera/CameraActor.h"

AMyPlayerController::AMyPlayerController()
{
	this->bShowMouseCursor = true;

	// Updating the drag and drop every 50ms is more than enough
	SetActorTickInterval(0.05);
}

void AMyPlayerController::ExitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();


	InputComponent->BindAction("MouseLeftClicked", IE_Pressed, this, &AMyPlayerController::LeftClickPressed);
	InputComponent->BindAction("MouseLeftClicked", IE_Released, this, &AMyPlayerController::LeftClickReleased);
	InputComponent->BindAction("Escape", IE_Pressed, this, &AMyPlayerController::ExitGame);
}

void AMyPlayerController::LeftClickPressed()
{
	// Get the location of the cursor in world space
	FVector Start;
	FVector ForwardVector;
	bool flag = DeprojectMousePositionToWorld(Start, ForwardVector);
	if (!flag) return;
	FVector End = ((ForwardVector * EDITING_RAY_LENGTH) + Start);

	// Spawn a ray from the cursor to "infinity" to find an editable table
	FCollisionQueryParams CollisionParams;
	FHitResult OutHit;
	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams) &&
		OutHit.bBlockingHit &&
		OutHit.GetActor()->GetClass() == AMyTableChair::StaticClass())
	{
		TableBeingEdited = (AMyTableChair*)OutHit.GetActor();
		CurrentCornerDraggedComponent = static_cast<UProceduralMeshComponent*>(OutHit.GetComponent());
	}
}

void AMyPlayerController::LeftClickReleased()
{
	TableBeingEdited = nullptr;
	CurrentCornerDraggedComponent = nullptr;
}


void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TableBeingEdited == nullptr || CurrentCornerDraggedComponent == nullptr)
		return;

	// Intersect the mouse ray with the table plane to find the new corner
	FVector Start;
	FVector ForwardVector;
	bool flag = DeprojectMousePositionToWorld(Start, ForwardVector);
	FVector End = ((ForwardVector * 10000.f) + Start);
	FVector NewCornerWorldLocation = FMath::LinePlaneIntersection(Start, End, CurrentCornerDraggedComponent->GetComponentLocation(), FVector::UpVector);

	TableBeingEdited->SetCornerWorldLocation(CurrentCornerDraggedComponent, NewCornerWorldLocation);
}