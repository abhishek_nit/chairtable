// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyTableChair.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CHAIRTABLEPROJ_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
private:


	AMyTableChair* TableBeingEdited;
	UProceduralMeshComponent* CurrentCornerDraggedComponent;

	void LeftClickPressed();
	void LeftClickReleased();
	void ExitGame();


public:

	AMyPlayerController();

	// The length of the ray to find an editable table
	static constexpr float EDITING_RAY_LENGTH = 10000.0f;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
};
