// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ChairTableProj/MyBoxComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBoxComponent() {}
// Cross Module References
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_UMyBoxComponent_NoRegister();
	CHAIRTABLEPROJ_API UClass* Z_Construct_UClass_UMyBoxComponent();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent();
	UPackage* Z_Construct_UPackage__Script_ChairTableProj();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMyBoxComponent::execSetBoxMaterial)
	{
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBoxMaterial(Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMyBoxComponent::execBuild)
	{
		P_GET_STRUCT(FVector,Z_Param_Size);
		P_GET_UBOOL(Z_Param_CollisionEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Build(Z_Param_Size,Z_Param_CollisionEnabled);
		P_NATIVE_END;
	}
	void UMyBoxComponent::StaticRegisterNativesUMyBoxComponent()
	{
		UClass* Class = UMyBoxComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Build", &UMyBoxComponent::execBuild },
			{ "SetBoxMaterial", &UMyBoxComponent::execSetBoxMaterial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMyBoxComponent_Build_Statics
	{
		struct MyBoxComponent_eventBuild_Parms
		{
			FVector Size;
			bool CollisionEnabled;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static void NewProp_CollisionEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CollisionEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyBoxComponent_eventBuild_Parms, Size), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size_MetaData)) };
	void Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled_SetBit(void* Obj)
	{
		((MyBoxComponent_eventBuild_Parms*)Obj)->CollisionEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled = { "CollisionEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MyBoxComponent_eventBuild_Parms), &Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_Build_Statics::NewProp_CollisionEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyBoxComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyBoxComponent_Build_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyBoxComponent, nullptr, "Build", nullptr, nullptr, sizeof(MyBoxComponent_eventBuild_Parms), Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_Build_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyBoxComponent_Build()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMyBoxComponent_Build_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics
	{
		struct MyBoxComponent_eventSetBoxMaterial_Parms
		{
			const UMaterialInterface* NewMaterial;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyBoxComponent_eventSetBoxMaterial_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::NewProp_NewMaterial,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyBoxComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMyBoxComponent, nullptr, "SetBoxMaterial", nullptr, nullptr, sizeof(MyBoxComponent_eventSetBoxMaterial_Parms), Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMyBoxComponent_NoRegister()
	{
		return UMyBoxComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMyBoxComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMyBoxComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProceduralMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ChairTableProj,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMyBoxComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMyBoxComponent_Build, "Build" }, // 1043613333
		{ &Z_Construct_UFunction_UMyBoxComponent_SetBoxMaterial, "SetBoxMaterial" }, // 715009743
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object LOD Mobility Trigger" },
		{ "IncludePath", "MyBoxComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "MyBoxComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMyBoxComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMyBoxComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMyBoxComponent_Statics::ClassParams = {
		&UMyBoxComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMyBoxComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMyBoxComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMyBoxComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMyBoxComponent, 2230276645);
	template<> CHAIRTABLEPROJ_API UClass* StaticClass<UMyBoxComponent>()
	{
		return UMyBoxComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMyBoxComponent(Z_Construct_UClass_UMyBoxComponent, &UMyBoxComponent::StaticClass, TEXT("/Script/ChairTableProj"), TEXT("UMyBoxComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMyBoxComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
