// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMaterialInterface;
struct FVector;
#ifdef CHAIRTABLEPROJ_MyBoxComponent_generated_h
#error "MyBoxComponent.generated.h already included, missing '#pragma once' in MyBoxComponent.h"
#endif
#define CHAIRTABLEPROJ_MyBoxComponent_generated_h

#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_SPARSE_DATA
#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetBoxMaterial); \
	DECLARE_FUNCTION(execBuild);


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetBoxMaterial); \
	DECLARE_FUNCTION(execBuild);


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyBoxComponent(); \
	friend struct Z_Construct_UClass_UMyBoxComponent_Statics; \
public: \
	DECLARE_CLASS(UMyBoxComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(UMyBoxComponent)


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMyBoxComponent(); \
	friend struct Z_Construct_UClass_UMyBoxComponent_Statics; \
public: \
	DECLARE_CLASS(UMyBoxComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(UMyBoxComponent)


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyBoxComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBoxComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBoxComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBoxComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBoxComponent(UMyBoxComponent&&); \
	NO_API UMyBoxComponent(const UMyBoxComponent&); \
public:


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBoxComponent(UMyBoxComponent&&); \
	NO_API UMyBoxComponent(const UMyBoxComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBoxComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBoxComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBoxComponent)


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_13_PROLOG
#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_SPARSE_DATA \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_RPC_WRAPPERS \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_INCLASS \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_SPARSE_DATA \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_INCLASS_NO_PURE_DECLS \
	ChairTable_Source_ChairTableProj_MyBoxComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAIRTABLEPROJ_API UClass* StaticClass<class UMyBoxComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ChairTable_Source_ChairTableProj_MyBoxComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
