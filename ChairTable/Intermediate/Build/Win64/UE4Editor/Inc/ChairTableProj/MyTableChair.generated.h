// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UProceduralMeshComponent;
class UMyBoxComponent;
struct FVector2D;
struct FVector;
#ifdef CHAIRTABLEPROJ_MyTableChair_generated_h
#error "MyTableChair.generated.h already included, missing '#pragma once' in MyTableChair.h"
#endif
#define CHAIRTABLEPROJ_MyTableChair_generated_h

#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_SPARSE_DATA
#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateLocations); \
	DECLARE_FUNCTION(execGetCounterClockwiseCorner); \
	DECLARE_FUNCTION(execGetClockwiseCorner); \
	DECLARE_FUNCTION(execGetCorners); \
	DECLARE_FUNCTION(execGetTableHeight); \
	DECLARE_FUNCTION(execGetTableSize); \
	DECLARE_FUNCTION(execGetOppositeCorner); \
	DECLARE_FUNCTION(execSetCornerWorldLocation);


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateLocations); \
	DECLARE_FUNCTION(execGetCounterClockwiseCorner); \
	DECLARE_FUNCTION(execGetClockwiseCorner); \
	DECLARE_FUNCTION(execGetCorners); \
	DECLARE_FUNCTION(execGetTableHeight); \
	DECLARE_FUNCTION(execGetTableSize); \
	DECLARE_FUNCTION(execGetOppositeCorner); \
	DECLARE_FUNCTION(execSetCornerWorldLocation);


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyTableChair(); \
	friend struct Z_Construct_UClass_AMyTableChair_Statics; \
public: \
	DECLARE_CLASS(AMyTableChair, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(AMyTableChair)


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMyTableChair(); \
	friend struct Z_Construct_UClass_AMyTableChair_Statics; \
public: \
	DECLARE_CLASS(AMyTableChair, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ChairTableProj"), NO_API) \
	DECLARE_SERIALIZER(AMyTableChair)


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyTableChair(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyTableChair) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTableChair); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTableChair); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTableChair(AMyTableChair&&); \
	NO_API AMyTableChair(const AMyTableChair&); \
public:


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyTableChair(AMyTableChair&&); \
	NO_API AMyTableChair(const AMyTableChair&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyTableChair); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyTableChair); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyTableChair)


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_PRIVATE_PROPERTY_OFFSET
#define ChairTable_Source_ChairTableProj_MyTableChair_h_13_PROLOG
#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_PRIVATE_PROPERTY_OFFSET \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_SPARSE_DATA \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_RPC_WRAPPERS \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_INCLASS \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ChairTable_Source_ChairTableProj_MyTableChair_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_PRIVATE_PROPERTY_OFFSET \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_SPARSE_DATA \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_INCLASS_NO_PURE_DECLS \
	ChairTable_Source_ChairTableProj_MyTableChair_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHAIRTABLEPROJ_API UClass* StaticClass<class AMyTableChair>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ChairTable_Source_ChairTableProj_MyTableChair_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
