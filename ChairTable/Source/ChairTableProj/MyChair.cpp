// Fill out your copyright notice in the Description page of Project Settings.


#include "MyChair.h"

// Sets default values
AMyChair::AMyChair()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	UE_LOG(LogTemp, Error, TEXT("AMyChair const."));
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialChair(TEXT("Material'/Game/xoio_berlinflat/Materials/fiberglas.fiberglas'"));
	if (MaterialChair.Succeeded())
	{
		Material = MaterialChair.Object;
		UE_LOG(LogTemp, Log, TEXT("Chair material loaded"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Chair material loading error"));
	}


	// Spawn the chair square
	auto ChairSquare = CreateDefaultSubobject<UMyBoxComponent>(TEXT("ChairSquare"));
	ChairSquare->Build(FVector(CHAIR_SQUARE_SIZE, CHAIR_SQUARE_SIZE, CHAIR_SQUARE_THICKNESS), false);
	ChairSquare->SetupAttachment(RootComponent);
	ChairSquare->SetRelativeLocation(FVector(0, 0, CHAIR_LEG_HEIGHT + CHAIR_SQUARE_THICKNESS / 2));
	ChairSquare->SetBoxMaterial(Material);


}

// Called when the game starts or when spawned
void AMyChair::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyChair::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

