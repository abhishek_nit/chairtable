// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/TargetPoint.h"



AMyGameModeBase::AMyGameModeBase() {
	PlayerControllerClass = AMyPlayerController::StaticClass();
	DefaultPawnClass = nullptr;
}
void AMyGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Targets;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Targets);

	if (Targets[0])
	{
		UE_LOG(LogTemp, Log, TEXT("Target exist"));
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Target not exist"));
	}
	AMyTableChair* Table = GetWorld()->SpawnActor<AMyTableChair>();
	Table->SetActorLocation(Targets[0]->GetActorLocation());

	UE_LOG(LogTemp, Log, TEXT("Actor Location: %s"), *Targets[0]->GetActorLocation().ToString());

	UE_LOG(LogTemp, Log, TEXT("AMyGameModeBase ok1"));
}