// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTableChair.h"

#ifdef UE_BUILD_DEVELOPMENT
#	include "Engine.h"
#endif

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

// Sets default values
AMyTableChair::AMyTableChair()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	DEFAULT_SIZE = FVector2D(100, 200);

	TableSize = DEFAULT_SIZE;
	// Create resizable corners
	Corners = {
		CreateDefaultSubobject<UMyBoxComponent>(TEXT("Corner1")),
		CreateDefaultSubobject<UMyBoxComponent>(TEXT("Corner2")),
		CreateDefaultSubobject<UMyBoxComponent>(TEXT("Corner3")),
		CreateDefaultSubobject<UMyBoxComponent>(TEXT("Corner4"))
	};
	for (size_t i = 0; i < Corners.Num(); i++)
	{
		Corners[i]->SetupAttachment(RootComponent);
		Corners[i]->Build(FVector(ANCHOR_SIZE, ANCHOR_SIZE, 1), true);
		Corners[i]->ContainsPhysicsTriMeshData(true);
	}
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialTileAnchor(TEXT("Material'/Game/Materials/M_TileAnchor.M_TileAnchor'"));
	if (MaterialTileAnchor.Succeeded())
	{
		for (size_t i = 0; i < Corners.Num(); i++)
		{
			Corners[i]->SetBoxMaterial(MaterialTileAnchor.Object);

			UE_LOG(LogTemp, Log, TEXT("Table constructed1"));
		}
		
	}



	Corners[0]->SetRelativeLocation(FVector(TableSize.X / 2, TableSize.Y / 2, LEG_LENGTH + TABLE_TOP_THICKNESS + ANCHOR_HOVER_DISTANCE));
	Corners[1]->SetRelativeLocation(FVector(-TableSize.X / 2, TableSize.Y / 2, LEG_LENGTH + TABLE_TOP_THICKNESS + ANCHOR_HOVER_DISTANCE));
	Corners[2]->SetRelativeLocation(FVector(-TableSize.X / 2, -TableSize.Y / 2, LEG_LENGTH + TABLE_TOP_THICKNESS + ANCHOR_HOVER_DISTANCE));
	Corners[3]->SetRelativeLocation(FVector(TableSize.X / 2, -TableSize.Y / 2, LEG_LENGTH + TABLE_TOP_THICKNESS + ANCHOR_HOVER_DISTANCE));


	// Load table material
	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/Materials/wood_chair.wood_chair'"));
	if (Material.Succeeded())
	{
		TableMaterial = Material.Object;
		UE_LOG(LogTemp, Log, TEXT("Table constructed2"));
	}
	
	// Create countertop
	TableTop = CreateDefaultSubobject<UMyBoxComponent>(TEXT("TableTop"));
	TableTop->SetupAttachment(RootComponent);
	TableTop->Build(FVector(TableSize, TABLE_TOP_THICKNESS), false);
	TableTop->SetBoxMaterial(TableMaterial);

	// Create Table Legs
	for (size_t i = 0; i < Corners.Num(); i++)
	{
		const FString LegName = "Leg" + FString::FromInt(i);
		auto LegComp = CreateDefaultSubobject<UMyBoxComponent>(*LegName);
		LegComp->Build(FVector(LEG_SIDE_SIZE, LEG_SIDE_SIZE, LEG_LENGTH), false);
		LegComp->SetupAttachment(RootComponent);
		LegComp->SetRelativeLocation(FVector::ZeroVector);
		LegComp->SetBoxMaterial(TableMaterial);
		Legs.Add(LegComp);
	}


	UE_LOG(LogTemp, Log, TEXT("Table constructed3"));
}

// Called when the game starts or when spawned
void AMyTableChair::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMyTableChair::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
FVector2D AMyTableChair::GetTableSize() const
{
	return TableSize;
}


void AMyTableChair::OnConstruction(const FTransform& Transform)
{
	UE_LOG(LogTemp, Log, TEXT("OnConstruction"));
	Super::OnConstruction(Transform);
	UpdateLocations();
}


float AMyTableChair::GetTableHeight() const
{
	return LEG_LENGTH + TABLE_TOP_THICKNESS;
}


TArray<UMyBoxComponent*> AMyTableChair::GetCorners() const
{
	return Corners;
}


UProceduralMeshComponent* AMyTableChair::GetOppositeCorner(const UProceduralMeshComponent* CurrentCorner) const
{
	// HACK: fix this with a lookup table maybe
	int index = Corners.IndexOfByKey(CurrentCorner);

	switch (index)
	{
	case 0:  return Corners[2];
	case 1:  return Corners[3];
	case 2:  return Corners[0];
	case 3:  return Corners[1];
	default: return nullptr;
	}
}

UProceduralMeshComponent* AMyTableChair::GetClockwiseCorner(const UProceduralMeshComponent* CurrentCorner) const
{
	// HACK: fix this with a lookup table maybe
	int index = Corners.IndexOfByKey(CurrentCorner);
	//return Corners[(index+1) % 4];
	switch (index)
	{
	case 0:  return Corners[1];
	case 1:  return Corners[2];
	case 2:  return Corners[3];
	case 3:  return Corners[0];
	default: return nullptr;
	}
}

UProceduralMeshComponent* AMyTableChair::GetCounterClockwiseCorner(const UProceduralMeshComponent* CurrentCorner) const
{
	// HACK: fix this with a lookup table maybe
	int index = Corners.IndexOfByKey(CurrentCorner);
	//return Corners[(index+1) % 4];
	switch (index)
	{
	case 0:  return Corners[3];
	case 1:  return Corners[0];
	case 2:  return Corners[1];
	case 3:  return Corners[2];
	default: return nullptr;
	}
}

bool AMyTableChair::SetCornerWorldLocation(UProceduralMeshComponent* Corner, const FVector NewCornerWorldLocation)
{
	if (Corner == nullptr)
		return false;

	const FVector OldLocation = Corner->GetComponentLocation();
	const UProceduralMeshComponent* OppositeCorner = GetOppositeCorner(Corner);
	const FVector OldSign = (Corner->GetRelativeTransform().GetLocation() - OppositeCorner->GetRelativeTransform().GetLocation()).GetSignVector();



	Corner->SetWorldLocation(NewCornerWorldLocation);


	UProceduralMeshComponent* ClockwiseCorner = GetClockwiseCorner(Corner);
	UProceduralMeshComponent* CounterClockwiseCorner = GetCounterClockwiseCorner(Corner);

	const FVector NewSign = (Corner->GetRelativeTransform().GetLocation() - OppositeCorner->GetRelativeTransform().GetLocation()).GetSignVector();


	UProceduralMeshComponent* Corner1;
	UProceduralMeshComponent* Corner2;

	auto CornerIndex = Corners.IndexOfByKey(Corner);

	switch (CornerIndex)
	{
	case 0:
	case 2:
		Corner1 = ClockwiseCorner;
		Corner2 = CounterClockwiseCorner;
		break;
	case 1:
	case 3:
		Corner1 = CounterClockwiseCorner;
		Corner2 = ClockwiseCorner;
		break;
	default:
		Corner1 = nullptr;
		Corner2 = nullptr;
	}
	const auto Corner1OldLoc = Corner1->GetComponentLocation();
	const auto Corner2OldLoc = Corner2->GetComponentLocation();
	Corner1->SetWorldLocation(FVector(Corner1->GetComponentLocation().X, NewCornerWorldLocation.Y, Corner1->GetComponentLocation().Z));
	Corner2->SetWorldLocation(FVector(NewCornerWorldLocation.X, Corner2->GetComponentLocation().Y, Corner2->GetComponentLocation().Z));


	if (FVector::Distance(NewCornerWorldLocation, ClockwiseCorner->GetComponentLocation()) < AMyTableChair::TABLE_MIN_SIZE ||
		FVector::Distance(NewCornerWorldLocation, CounterClockwiseCorner->GetComponentLocation()) < AMyTableChair::TABLE_MIN_SIZE ||
		FVector::Distance(NewCornerWorldLocation, ClockwiseCorner->GetComponentLocation()) > AMyTableChair::TABLE_MAX_SIZE ||
		FVector::Distance(NewCornerWorldLocation, CounterClockwiseCorner->GetComponentLocation()) > AMyTableChair::TABLE_MAX_SIZE ||
		OldSign != NewSign
		)
	{
		Corner->SetWorldLocation(OldLocation);
		Corner1->SetWorldLocation(Corner1OldLoc);
		Corner2->SetWorldLocation(Corner2OldLoc);
		return false;
	}

	// Update the table vertexes
	UpdateLocations();

	return true;
}

void AMyTableChair::UpdateLocations()
{
	
	// Cache table size
	TableSize = FVector2D(
		FVector::Distance(Corners[0]->GetComponentLocation(), Corners[1]->GetComponentLocation()),
		FVector::Distance(Corners[0]->GetComponentLocation(), Corners[3]->GetComponentLocation())
	);

	UE_LOG(LogTemp, Log, TEXT("Corners[0]:%s"),*Corners[0]->GetComponentLocation().ToString());
	UE_LOG(LogTemp, Log, TEXT("Corners[1]:%s"),*Corners[1]->GetComponentLocation().ToString());

	UE_LOG(LogTemp, Log, TEXT("Corners[2]:%s"),*Corners[2]->GetComponentLocation().ToString());

	UE_LOG(LogTemp, Log, TEXT("Corners[3]:%s"),*Corners[3]->GetComponentLocation().ToString());


	FVector NewRelativeRoot = FVector(
		Corners[0]->GetRelativeTransform().GetLocation().X - TableSize.X / 2,
		Corners[0]->GetRelativeTransform().GetLocation().Y - TableSize.Y / 2,
		0
	);
	FVector NewWorldRoot = this->GetTransform().TransformPosition(NewRelativeRoot);


	TableTop->Build(FVector(TableSize, TABLE_TOP_THICKNESS), false);
	TableTop->SetWorldLocation(NewWorldRoot + FVector(0, 0, LEG_LENGTH + TABLE_TOP_THICKNESS / 2));



	// Update Table Legs location
	const TArray<FVector> LegsOffsets = {
	{-LEG_SIDE_SIZE / 2, -LEG_SIDE_SIZE / 2, -LEG_LENGTH / 2 - ANCHOR_HOVER_DISTANCE - TABLE_TOP_THICKNESS},
	{ LEG_SIDE_SIZE / 2, -LEG_SIDE_SIZE / 2, -LEG_LENGTH / 2 - ANCHOR_HOVER_DISTANCE - TABLE_TOP_THICKNESS},
	{LEG_SIDE_SIZE / 2, LEG_SIDE_SIZE / 2, -LEG_LENGTH / 2 - ANCHOR_HOVER_DISTANCE - TABLE_TOP_THICKNESS},
	{-LEG_SIDE_SIZE / 2, LEG_SIDE_SIZE / 2, -LEG_LENGTH / 2 - ANCHOR_HOVER_DISTANCE - TABLE_TOP_THICKNESS} };
	for (size_t i = 0; i < 4; i++)
		Legs[i]->SetRelativeLocation(Corners[i]->GetRelativeTransform().GetLocation() + LegsOffsets[i]);



	// Remove old chairs
	TArray<AActor*> SpawnedChairs;
	this->GetAllChildActors(SpawnedChairs);
	for (size_t i = 0; i < SpawnedChairs.Num(); i++)
		SpawnedChairs[i]->Destroy();

	// Spawn necessary chairs
	constexpr float CHAIRS_DISTANCE_FROM_TABLE = 25;
	constexpr float DISTANCE_BETWEEN_CHAIRS = 50;
	constexpr float CHAIRS_INTERVAL = (AMyChair::CHAIR_SQUARE_SIZE + DISTANCE_BETWEEN_CHAIRS);



	int ChairsToSpawnOnYSide = TableSize.Y / CHAIRS_INTERVAL;
	int ChairsToSpawnOnXSide = TableSize.X / CHAIRS_INTERVAL;

	// The offset to add so the chairs line is centered
	// Remaining space / 2
	const float CHAIRS_YLINE_LENGTH = ChairsToSpawnOnYSide * AMyChair::CHAIR_SQUARE_SIZE + (ChairsToSpawnOnYSide - 1) * DISTANCE_BETWEEN_CHAIRS;
	const float YChairsSpawnOffset = (TableSize.Y - CHAIRS_YLINE_LENGTH) / 2;


	// Backward side
	FVector ChairsForwardSpawnPoint = Corners[2]->GetComponentLocation();
	ChairsForwardSpawnPoint.X -= CHAIRS_DISTANCE_FROM_TABLE;
	ChairsForwardSpawnPoint.Y += AMyChair::CHAIR_SQUARE_SIZE / 2;
	ChairsForwardSpawnPoint.Z = this->GetActorLocation().Z;

	for (int y = 0; y < ChairsToSpawnOnYSide; y++)
	{
		UChildActorComponent* NewChair = NewObject<UChildActorComponent>(this);
		NewChair->SetupAttachment(RootComponent);
		NewChair->SetChildActorClass(AMyChair::StaticClass());
		NewChair->SetWorldLocation(FVector(ChairsForwardSpawnPoint.X, YChairsSpawnOffset + ChairsForwardSpawnPoint.Y + y * CHAIRS_INTERVAL, this->GetActorLocation().Z));
	}
	RegisterAllComponents();
}