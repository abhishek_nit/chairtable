// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Materials/Material.h"
#include "MyBoxComponent.h"
#include "MyChair.h"
#include "MyTableChair.generated.h"

UCLASS(Blueprintable)
class CHAIRTABLEPROJ_API AMyTableChair : public AActor
{
	GENERATED_BODY()

private:

	TArray<UMyBoxComponent*> Corners;

	UMyBoxComponent* TableTop;

	TArray<UMyBoxComponent*> Legs;

	
public:	

	  float TABLE_TOP_THICKNESS = 10.0f;
	  float LEG_LENGTH = 65;
	  float LEG_SIDE_SIZE = 10;
	  FVector2D DEFAULT_SIZE;
	  float ANCHOR_SIZE = 25;
	  float ANCHOR_HOVER_DISTANCE = 2;
	  float TABLE_MIN_SIZE = 50.0f;
	  float TABLE_MAX_SIZE = 5000.0f;
	// Sets default values for this actor's properties
	FVector2D TableSize ;
	AMyTableChair();

protected:

	virtual void OnConstruction(const FTransform& Transform) override;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	bool SetCornerWorldLocation(UProceduralMeshComponent* Corner, const FVector NewWorldLocation);

	UFUNCTION(BlueprintPure)
	UProceduralMeshComponent* GetOppositeCorner(const UProceduralMeshComponent* CurrentCorner) const;

	UFUNCTION(BlueprintPure)
	FVector2D GetTableSize() const;

	UFUNCTION(BlueprintPure)
	float GetTableHeight() const;

	UFUNCTION(BlueprintPure)
	TArray<UMyBoxComponent*> GetCorners() const;

	UFUNCTION(BlueprintPure)
	UProceduralMeshComponent* GetClockwiseCorner(const UProceduralMeshComponent* const CurrentCorner) const;

	UFUNCTION(BlueprintPure)
	UProceduralMeshComponent* GetCounterClockwiseCorner(const UProceduralMeshComponent* const CurrentCorner) const;


	UFUNCTION(BlueprintCallable)
	void UpdateLocations();


	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UMaterial* TableMaterial;
};
